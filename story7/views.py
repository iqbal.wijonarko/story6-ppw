from django.shortcuts import render

# Create your views here.
def nightPage(request):
    return render(request, 'nightmode.html')
