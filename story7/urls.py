from django.urls import path
from .views import nightPage

app_name = 'story7'

urlpatterns = [
    path('', views.nightPage, name='nightPage')
]