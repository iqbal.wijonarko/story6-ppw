from django.shortcuts import render

# Create your views here.
def searchBookPage(request):
    return render(request, 'searchbook.html')
