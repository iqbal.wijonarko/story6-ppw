from django.urls import path
from .views import searchBookPage

app_name = 'story8'

urlpatterns = [
    path('', views.searchBookPage, name='searchBookPage')
]