from django import forms
from . import models

class createStatus(forms.ModelForm):
    class Meta:
        model = models.statusModel
        fields = ['status']