from django.urls import path, include
from . import views
from story7.views import nightPage
from story8.views import searchBookPage

app_name = 'landingpage'

urlpatterns = [
    path('', views.statusPage, name='statusPage'),
    path('nightPage/', nightPage, name='nightPage'),
    path('searchBook/', searchBookPage, name='searchBookPage'),
    
]