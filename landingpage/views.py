from django.shortcuts import render
from django.shortcuts import redirect
from .forms import createStatus
from .models import statusModel

# Create your views here.

def statusPage(request):
    # status = createStatus.objects.all()
    if request.method == 'POST':
        form = createStatus(request.POST)
        if form.is_valid():
            form.save()
            return redirect('landingpage:statusPage')
    else:
        form = createStatus()
    status = statusModel.objects.all()
    return render(request, 'status.html', {'form': form, 'status': status})

    
