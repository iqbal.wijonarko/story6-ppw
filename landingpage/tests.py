from django.test import TestCase, LiveServerTestCase
from django.test import SimpleTestCase
from django.urls import resolve
from django.http import HttpRequest
from landingpage.forms import createStatus
from landingpage.models import statusModel
from landingpage.views import statusPage
from django.test import Client
from django.urls import reverse, resolve
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time
# import json
from . import forms
from . import views

# Create your tests here.
class statusPageTest(TestCase):

    def test_no_data(self):
        form = createStatus(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors),1)

    def test_form_valid(self):
        form = createStatus(data={
            'status':'halo'})
        self.assertTrue(form.is_valid())

    def test_url_exist(self):
        status = statusModel.objects.create(
            status = "halo"
        )
        response = self.client.get('')
        self.assertEqual(response.status_code,200)

    def setUp(self):
        self.project = statusModel.objects.create(
            status = 'halo'
        )
        self.project1 = statusModel.objects.create(
            status = 'halo'
        )

        self.client = Client()
        self.tdd_url = reverse('landingpage:statusPage')

    def test_tdd_GET(self):
        response = self.client.get(self.tdd_url, {"form":'status'})
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'status.html')

    def test_tdd_POST_greet(self):
        status_data = {
            'status':'halo'
        }
        form = forms.createStatus(data = status_data)
        self.assertTrue(form.is_valid())
        request = self.client.post(self.tdd_url, data = status_data)
        self.assertEquals(request.status_code, 302)

        response = self.client.get(self.tdd_url)
        self.assertEquals(response.status_code, 200)

    def test_project(self):
        self.assertNotEqual(self.project, self.project1)

    def test_tdd_url_is_resolved(self):
        url = reverse('landingpage:statusPage')
        self.assertEquals(resolve(url).func , statusPage)

class functionTestPage(LiveServerTestCase):

    def setUp(self):
        super(functionTestPage, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)

    def test_response(self):
        status = statusModel.objects.create(status = "Halo")
        self.browser.get(self.live_server_url)
        kontain = self.browser.find_element_by_class_name('status')
        self.assertEqual(kontain.find_element_by_tag_name('h1').text, "Halo")

    def tearDown(self):
        self.browser.quit()
        super(functionTestPage, self).tearDown()


