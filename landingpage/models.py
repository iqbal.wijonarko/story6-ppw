from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class statusModel(models.Model):
    status = models.TextField(blank = False, null = False, max_length = 50)
    date = models.DateTimeField(auto_now_add=True, blank=True)