from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User



def accountPage(request):
    context = { 
        'greeting' : '',
        'please' : '' ,
        'log' : '',
    }
    if request.user.is_authenticated:
        context['greeting'] = 'Welcome  ' + request.session['username']
        context['please'] = 'Click button to logout'
        context['log'] = 'logout'
        return render(request, 'login.html', context)
    else:
        return render(request, 'account.html')


def loginView(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect('login')
        else:
            return render(request, 'account.html')

    if request.method == "POST":
        if len(request.POST) == 3:
            username_login = request.POST['username']
            password_login = request.POST['password']
                    
            user = authenticate(request, username=username_login, password=password_login)
            if user is not None:
                login(request, user)
                request.session['username'] = user.username
                return redirect('login')
                
            else:
                return redirect('accountPage')

        elif len(request.POST) == 4:
            username_login = request.POST['username']
            email_login = request.POST['email']
            password_login = request.POST['password']
            users = User.objects.all()

            for user in users:
                if user.username == username_login :
                    return redirect('accountPage')

            user = User.objects.create_user(username=username_login, email=email_login, password=password_login)

            login(request, user)
            request.session['username'] = user.username
            

        return redirect('login')




def logoutView(request):
    request.session.flush()
    if request.method == "GET":
        if request.user.is_authenticated:
            logout(request)
    return redirect('accountPage')













