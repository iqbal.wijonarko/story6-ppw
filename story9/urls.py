from django.urls import path
from .views import accountPage, loginView, logoutView



urlpatterns = [
    path('',loginView , name='accountPage'),
    path('login/', accountPage, name='login'),
    path('logout', logoutView, name='logout'),
]