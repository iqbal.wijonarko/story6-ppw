from django.test import TestCase
from django.test import Client
from django.urls import reverse, resolve
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import json
import time
from selenium.webdriver.chrome.options import Options
from .views import *;
from selenium import webdriver

class UnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.authpage_login = reverse("login")


    def test_url_exist(self):
        response = Client().get(self.authpage_login)
        self.assertEqual(response.status_code, 200)


    def test_url_not_exist(self):
        response = Client().get('/test')
        self.assertEqual(response.status_code, 404)

    def test_page(self):
        response = Client().get(self.authpage_login)
        self.assertTemplateUsed(response, 'account.html')




class TestFunctionalPage(StaticLiveServerTestCase):

    def setUp(self):

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage') 
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TestFunctionalPage, self).setUp()

    def tearDown(self):

        self.browser.quit()
        super(TestFunctionalPage, self).tearDown()

    def test_title_element_in_project(self):
        self.browser.get(self.live_server_url + "/account")

        title = self.browser.find_element_by_id('footerText')
        self.assertIn("Already Member?", title.text)
       
        time.sleep(10)

    def test_can_login(self):
        # Create user
        User.objects.create_user(username='iqbale', email='iqbale@email.com', password='12345')

        self.browser.get(self.live_server_url + "/account")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('button')

        username.send_keys('iqbale')
        password.send_keys('12345')
        submit.click()
        time.sleep(10)

        title = self.browser.find_element_by_tag_name('h1')
        self.assertIn("WELCOME IQBALE", title.text)
        title2 = self.browser.find_element_by_tag_name('h2')
        self.assertIn("Click button to logout", title2.text)
        # self.assertIn("WELCOME IQBALE", self.browser.page_source)
        # self.assertIn("Click button to logout", self.browser.page_source)
        time.sleep(10)

    # def test_can_signup(self):
    #     self.browser.get(self.live_server_url + "/account")

    #     title = self.browser.find_element_by_id('tab-2')
    #     username = self.browser.find_element_by_id('usernameUp')
    #     password = self.browser.find_element_by_id('passwordUp')
    #     email = self.browser.find_element_by_id('emailUp')
    #     submit = self.browser.find_element_by_id('buttonUp')

    #     title.get(0).click()
    #     title.checked = True
    #     username.send_keys('iqbale')
    #     password.send_keys('12345')
    #     email.send_keys('iqbale@email.com')
    #     submit.click()
    #     time.sleep(10)

    #     title = self.browser.find_element_by_tag_name('h1')
    #     self.assertIn("WELCOME IQBALE", title.text)
    #     title2 = self.browser.find_element_by_tag_name('h2')
    #     self.assertIn("Click button to logout", title2.text)
    #     # self.assertIn("WELCOME IQBALE", self.browser.page_source)
    #     # self.assertIn("Click button to logout", self.browser.page_source)

    #     time.sleep(10)

    def test_auth_can_access_logout(self):
        # Create user
        User.objects.create_user(username='iqbale', email='iqbale@email.com', password='12345')

        self.browser.get(self.live_server_url + "/account/")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('button')

        username.send_keys('iqbale')
        password.send_keys('12345')
        submit.click()
        time.sleep(10)

        logout = self.browser.find_element_by_id('logout')
        logout.click()
        time.sleep(10)

        title = self.browser.find_element_by_id('footerText')
        self.assertIn("Already Member?", title.text)
        time.sleep(10)

    def test_login_with_wrong_username(self):
       self.browser.get(self.live_server_url + "/account")



       username = self.browser.find_element_by_id('username_login')
       password = self.browser.find_element_by_id('password_login')
       submit = self.browser.find_element_by_id('button')

       username.send_keys('iqbale')
       password.send_keys('12345')
       submit.click()
       time.sleep(10)
       self.authpage_login = reverse("login")
       response = Client().get(self.authpage_login)



       self.assertTemplateUsed(response, 'account.html')
       time.sleep(10)

    # def test_signup_with_same_username(self):
    #     # Create user
    #     User.objects.create_user(username='iqbale', email='iqbale@email.com', password='12345')

    #     self.browser.get(self.live_server_url + "/account")



    #     username = self.browser.find_element_by_id('usernameUp')
    #     email = self.browser.find_element_by_id('emailUp')
    #     password = self.browser.find_element_by_id('passwordUp')
    #     submit = self.browser.find_element_by_id('buttonUp')

    #     username.send_keys('iqbale')
    #     email.send_keys('iqbale@email.com')
    #     password.send_keys('12345')
    #     submit.click()
    #     time.sleep(10)
    #     self.authpage_login = reverse("accountPage")
    #     response4 = Client().get(self.authpage_login)



    #     self.assertTemplateUsed(response4, 'account.html')

    #     time.sleep(10)

    def test_auth_cannot_access_login(self):
        # Create user
        User.objects.create_user(username='iqbale', email='iqbale@email.com', password='12345')

        self.browser.get(self.live_server_url + "/account")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('button')

        username.send_keys('iqbale')
        password.send_keys('12345')
        submit.click()
        time.sleep(10)

        self.browser.get(self.live_server_url + "/account")

        title = self.browser.find_element_by_tag_name('h1')
        self.assertIn("WELCOME IQBALE", title.text)
        title2 = self.browser.find_element_by_tag_name('h2')
        self.assertIn("Click button to logout", title2.text)
        # self.assertIn("Click button to logout", self.browser.page_source)
        time.sleep(10)