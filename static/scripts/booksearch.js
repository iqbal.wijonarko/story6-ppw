function bookSearch(){

    var searchBox = document.getElementById('searchBox').value
    document.getElementById('searchResult').innerHTML = ""
    console.log(searchBox)


    $.ajax({

        url:"https://www.googleapis.com/books/v1/volumes?q=" + searchBox,
        dataType: "json",

        success: function(data) {
            for (i = 0; i < data.items.length ; i++) {

                if(data.items[i].volumeInfo.imageLinks == null){

                    searchResult.innerHTML += 
                    "<div class='col-sm-3 justify-content-center' align='center' id='formBox'>" +
                        "<a class='event text-center'  href='"+data.items[i].volumeInfo.previewLink+"' target='_blank' style='background-image: url(https://icon-library.net/images/not-found-icon/not-found-icon-28.jpg)' >" +
                        "</a>" +
                        "<br>" +
                        "<div class='bookInfo'>" +
                            "<h4> <Strong>Title</Strong> : " + data.items[i].volumeInfo.title +  "</h4>" +
                            "<h4> <Strong>Author</Strong> : " + data.items[i].volumeInfo.authors +  "</h4>" +
                            "<h4> <Strong>Published Date</Strong> : " + data.items[i].volumeInfo.publishedDate +  "</h4>" +
                        "</div>" +
                    "</div>";
                }
            else{
                    searchResult.innerHTML +=   
                    "<div class='col-sm-3 justify-content-center' align='center' id='formBox'>" +
                        "<a class='event text-center'  href='"+data.items[i].volumeInfo.previewLink+"' target='_blank' style='background-image: url("+ data.items[i].volumeInfo.imageLinks.smallThumbnail +")' >" +
                        "</a>" +
                        "<br>" +
                        "<div class='bookInfo'>" +
                            "<h4> <Strong>Title</Strong> : " + data.items[i].volumeInfo.title +  "</h4>" +
                            "<h4> <Strong>Author</Strong> : " + data.items[i].volumeInfo.authors +  "</h4>" +
                            "<h4> <Strong>Published Date</Strong> : " + data.items[i].volumeInfo.publishedDate +  "</h4>" +
                        "</div>" +
                    "</div>";
                }
            }
        },
        type: 'GET'
    });
}

$.ajax({
    
    url:"https://www.googleapis.com/books/v1/volumes?q=Sherlock Holmes",
    dataType: "json",
    
    success: function(data) {
        for (i = 0; i < data.items.length ; i++) {
            
            if(data.items[i].volumeInfo.imageLinks == null){
                
                searchResult.innerHTML += 
                "<div class='col-sm-3 justify-content-center' align='center' id='formBox'>" +
                    "<a class='event text-center'  href='"+data.items[i].volumeInfo.previewLink+"' target='_blank' style='background-image: url(https://icon-library.net/images/not-found-icon/not-found-icon-28.jpg)' >" +
                    "</a>" +
                    "<br>" +
                    "<div class='bookInfo'>" +
                        "<h4> <Strong>Title</Strong> : " + data.items[i].volumeInfo.title +  "</h4>" +
                        "<h4> <Strong>Author</Strong> : " + data.items[i].volumeInfo.authors +  "</h4>" +
                        "<h4> <Strong>Published Date</Strong> : " + data.items[i].volumeInfo.publishedDate +  "</h4>" +
                    "</div>" +
                "</div>";
            }
            else{
                searchResult.innerHTML +=  
                "<div class='col-sm-3 justify-content-center' align='center' id='formBox'>" +
                    "<a class='event text-center'  href='"+data.items[i].volumeInfo.previewLink+"' target='_blank' style='background-image: url("+ data.items[i].volumeInfo.imageLinks.smallThumbnail +")' >" +
                    "</a>" +
                    "<br>" +
                    "<div class='bookInfo'>" +
                        "<h4> <Strong>Title</Strong> : " + data.items[i].volumeInfo.title +  "</h4>" +
                        "<h4> <Strong>Author</Strong> : " + data.items[i].volumeInfo.authors +  "</h4>" +
                        "<h4> <Strong>Published Date</Strong> : " + data.items[i].volumeInfo.publishedDate +  "</h4>" +
                    "</div>" +
                "</div>";
            }
        }
    },
    type: 'GET'
});

document.getElementById('button').addEventListener('click', bookSearch, false)